 
##Unreal Engine 4 for Universal Windows Platform##
***Readme Updated 2016.07.20 @ 1:46PM***

**Original UE4 Readme.MD is here:**  https://github.com/EpicGames/UnrealEngine/blob/release/README.md

###Summary###
Microsoft has developed Universal Windows Platform (UWP) support for Unreal Engine 4, and has released the source code on GitHub as a fork of Epic Games' UE4 repository.  This code is now available to all UE4 licensees under the terms of the UE4 license, which provide for source code redistribution and use.

 - **This code is provided by Microsoft “as is” with no warranty.**
 - This information and other FAQ updates will be published to the Readme.MD file inside the repository itself.  Follow the instructions below to get access.  

###Instructions to share with others###
Access the UE4 UWP fork here:  https://github.com/MICROSOFT-XBOX-ATG/MICROSOFT_UWP_UNREAL

If you see a 404 page, you need to complete the enrollment process:

1.	[Create a GitHub account.](http://www.github.com/)
2.	[Sign up for the Epic program](https://github.com/EpicGames/Signup)
3.	[Follow the instructions to associate your Epic Program account with your GitHub account.](https://www.unrealengine.com/ue4-on-github)
4.	[Join the Epic GitHub Org (from the email invitation you receive after Step 3)](https://github.com/EpicGames/UnrealEngine)
5.	[Done!  You should be able to access the Microsoft UWP Unreal fork on GitHub](https://github.com/MICROSOFT-XBOX-ATG/MICROSOFT_UWP_UNREAL) and the [Readme.MD](https://github.com/MICROSOFT-XBOX-ATG/MICROSOFT_UWP_UNREAL/blob/release_uwp/README.md) with more information.

###Community & Support###
This project is community-supported, with periodic updates from Microsoft’s Xbox Advanced Technology Group (ATG) to help accelerate game development for UWP.  
 - Feel free to join the discussion on the Unreal Engine forums at:  https://forums.unrealengine.com/forumdisplay.php?29-Engine-Source-amp-GitHub.  
 - Xbox program partners can ask questions on the forums at https://developer.xboxlive.com.  

###Getting Started with UWP Game Development###
 - New to UWP?  A general overview of UWP development, tooling (packaging, debugging), and requirements (Store configuration, publishing, etc.) is here:  https://developer.microsoft.com/en-us/windows/games/getstarted.  
 - Ready to build UWP games?  More game-specific, end to end UWP development guidance - curated by game developers for game developers - is included in the link above, but you can go directly to that guide here:  https://msdn.microsoft.com/windows/uwp/gaming/e2e

###Xbox Live###
Xbox program partners can also request an Xbox Live-ready UWP branch of the Unreal 4 Engine through their Microsoft Developer Account Manager (DAM).

________________________________________

DISCLAIMER

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE
